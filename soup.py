#!/bin/env python3

"""
Copyright 2021 cyberia <cyberia@donk.systems>. Licence GPL-3.0 or later.

soup: a basic failure-propagating process supervisor.

It spawns the provided arguments in a shell, and terminates all its children as
soon as one terminates.
"""

from errno import ECHILD
from os import waitpid, WNOHANG
from signal import sigwait, SIGCHLD, SIGTERM, valid_signals, pthread_sigmask, SIG_BLOCK, SIG_SETMASK
from subprocess import Popen
from sys import argv, exit, stderr

if len(argv) < 2:
    exit('Must provide at least one process to spawn')

signals_to_intercept = {SIGCHLD, SIGTERM}

old_sigmask = pthread_sigmask(SIG_BLOCK, signals_to_intercept)

children = {}
for cmd in argv[1:]:
    child = Popen(cmd, shell=True, preexec_fn=lambda: pthread_sigmask(SIG_SETMASK, old_sigmask))
    children[child.pid] = child

def wait_all():
    done_waiting = False
    combined_exit_code = 0
    any_child_terminated = False
    while not done_waiting:
        try:
            (pid, exitcode) = waitpid(-1, WNOHANG)
        except ChildProcessError as e:
            if e.errno == ECHILD:
                done_waiting = True
                continue
            raise
        if pid == 0:
            break
        elif pid not in children:
            print(f"waitpid gave us a PID that we didn't spawn... {pid}", file=stderr)
        else:
            print(f"Child {(pid, children[pid].args)} exited with {exitcode}")
            combined_exit_code |= exitcode
            any_child_terminated = True
    return (any_child_terminated, combined_exit_code)

supervisor_exitcode = 0
should_exit = False
while not should_exit:
    print("Waiting for SIGCHLD or SIGTERM")
    sig = sigwait(signals_to_intercept)
    if sig == SIGCHLD:
        print("Received SIGCHLD")
        (any_child_terminated, supervisor_exitcode) = wait_all()
        should_exit = any_child_terminated
    elif sig == SIGTERM:
        print("Received SIGTERM")
        should_exit = True

print("Terminating all children...")

failed_terminating = False
for child in children.values():
    print(f"Sending SIGTERM to child {(child.pid, child.args)}... ", end='', flush=True)
    try:
        child.terminate()
        print("done")
    except e:
        print(f"Error terminating child {child.pid}: {e}", file=stderr)
        failed_terminating = True

if failed_terminating:
    exit('Failed during child termination')

for child in children.values():
    print(f"Waiting on child {(child.pid, child.args)}... ", end='', flush=True)
    child.wait()
    print("done")

print(f"Combined exitcode {supervisor_exitcode}")

print(f"Goodbye")

exit(supervisor_exitcode)
