# soup

A basic failure-propagating process supervisor.

You call it like this:

```
./soup 'sleep 1' 'sleep 10'
```

When either `sleep 1` or `sleep 10` exits, SIGTERM is sent to all subcommands,
and the main process waits on them all to terminate.

The exit code of the main process will be the combined exit codes of the
initial failed subprocess(es).

Because the subprocesses are spawned inside shells, this is not
necessarily the exit code of the actual commands, but for most shells it is
non-zero if the commands returned non-zero.

When the main process itself receives SIGTERM, it propagates the signal out to
its subprocesses and terminates them as if one of them had failed. In this case
the exit code is 0.

## C and Python versions

They should be equivalent. I wrote it in Python initially, and then thought it
would be easy enough to port to C.

## Licence

GPL-3.0 or later
