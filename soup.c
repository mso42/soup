/* Copyright 2021 cyberia <cyberia@donk.systems>. Licence GPL-3.0 or later.
 *
 * soup: a basic failure-propagating process supervisor
 *
 * It spawns the provided arguments in a shell, and terminates all its children
 * as soon as one terminates.
 */

#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

typedef struct child_info {
  pid_t pid;
  char *cmd;
} child_info;

#define MAX_CHILDREN 1024

static child_info children[MAX_CHILDREN];
static size_t n_children = 0;

int spawn_child (char *cmd, sigset_t *child_sigmask) {
  char *shell = getenv("SHELL");
  if (shell == NULL) {
    shell = "/bin/sh";
  }

  pid_t pid = fork();
  if (pid == -1) {
    perror("fork");
    return -1;
  }

  if (pid == 0) {
    // Child
    if (pthread_sigmask(SIG_SETMASK, child_sigmask, NULL) == -1) {
      perror("pthread_sigmask");
      exit(EXIT_FAILURE);
    }

    if (execl(shell, shell, "-c", cmd, NULL) == -1) {
      perror("execl");
      exit(EXIT_FAILURE);
    }
    assert(false); // No return from execl
  } else {
    // Parent
    children[n_children].pid = pid;
    children[n_children].cmd = cmd;
    n_children++;
    return 0;
  }
}

void terminate_all () {
  printf("Terminating all children...\n");

  bool failed_sending_sigterm = false;
  for (size_t idx = 0; idx < n_children; idx++) {
    printf("Sending SIGTERM to child (%d, %s)... ", children[idx].pid, children[idx].cmd);
    fflush(stdout);
    if (kill(children[idx].pid, SIGTERM) == -1) {
      if (errno == ESRCH) {
        printf("does not exist\n");
        continue;
      }
      fprintf(stderr, "Error terminating pid %d: ", children[idx].pid);
      perror("kill");
      failed_sending_sigterm = true;
    } else {
      printf("succeeded\n");
    }
  }
  if (failed_sending_sigterm) {
    fprintf(stderr, "Exiting early because of failure to kill some children\n");
    exit(EXIT_FAILURE);
  }

  bool failed_waiting = false;
  for (size_t idx = 0; idx < n_children; idx++) {
    printf("Waiting on child (%d, %s)... ", children[idx].pid, children[idx].cmd);
    fflush(stdout);
    if (waitpid(children[idx].pid, NULL, 0) == -1) {
      if (errno == ECHILD) {
        printf("does not exist\n");
        continue;
      }
      fprintf(stderr, "Error waiting on pid %d: ", children[idx].pid);
      perror("waitpid");
      failed_waiting = true;
    } else {
      printf("succeeded\n");
    }
  }
  if (failed_waiting) {
    fprintf(stderr, "Exiting early because of failure to wait on some children\n");
    exit(EXIT_FAILURE);
  }

  return;
}

child_info* lookup_child (pid_t pid) {
  for (size_t idx = 0; idx < n_children; idx++) {
    if (children[idx].pid == pid) {
      return &children[idx];
    }
  }
  return NULL;
}

int wait_all (bool *any_child_terminated, int *combined_exit_code) {
  bool done_waiting = false;
  *combined_exit_code = 0;
  while (!done_waiting) {
    int stat_val;
    pid_t pid = waitpid(-1, &stat_val, WNOHANG);
    if (pid == -1) {
      if (errno == ECHILD) {
        done_waiting = true;
      } else {
        perror("waitpid");
        return -1;
      }
    } else if (pid == 0) {
      done_waiting = true;
    } else {
      child_info *child = lookup_child(pid);
      if (child == NULL) {
        fprintf(stderr, "waitpid gave us a PID that we didn't spawn... %d\n", pid);
      } else {
        int exitcode = WEXITSTATUS(stat_val);
        printf("Child (%d, %s) exited with %d\n", pid, child->cmd, exitcode);
        *combined_exit_code |= exitcode;
        *any_child_terminated = true;
      }
    }
  }
  return 0;
}

int main (int argc, char **argv) {
  if (argc < 2) {
    fprintf(stderr, "Must provide at least one process to spawn\n");
    exit(EXIT_FAILURE);
  }

  if (argc > MAX_CHILDREN + 1) {
    fprintf(stderr, "MAX_CHILDREN (%d) exceeded\n", MAX_CHILDREN);
    exit(EXIT_FAILURE);
  }

  sigset_t signals_to_intercept;
  sigset_t old_sigmask;
  sigemptyset(&signals_to_intercept);
  if (sigaddset(&signals_to_intercept, SIGCHLD) == -1) {
    perror("sigaddset");
    exit(EXIT_FAILURE);
  }
  if (sigaddset(&signals_to_intercept, SIGTERM) == -1) {
    perror("sigaddset");
    exit(EXIT_FAILURE);
  }

  if (pthread_sigmask(SIG_BLOCK, &signals_to_intercept, &old_sigmask) == -1) {
    perror("pthread_sigmask");
    exit(EXIT_FAILURE);
  }

  for (size_t idx = 1; idx < argc; idx++) {
    if (spawn_child(argv[idx], &old_sigmask) == -1) {
      terminate_all();
      exit(EXIT_FAILURE);
    }
  }

  int supervisor_exitcode = 0;
  bool should_exit = false;
  while (!should_exit) {
    printf("Waiting for SIGCHLD or SIGTERM\n");
    int signal;
    if (sigwait(&signals_to_intercept, &signal) == -1) {
      perror("sigwait");
      terminate_all();
      exit(EXIT_FAILURE);
    }
    if (signal == SIGCHLD) {
      printf("Received SIGCHLD\n");
      if (wait_all(&should_exit, &supervisor_exitcode) == -1) {
        terminate_all();
        exit(EXIT_FAILURE);
      }
    } else if (signal == SIGTERM) {
      printf("Received SIGTERM\n");
      should_exit = true;
    }
  }

  terminate_all();
  printf("Combined exitcode %d\n", supervisor_exitcode);
  printf("Goodbye\n");
  exit(supervisor_exitcode);
}
